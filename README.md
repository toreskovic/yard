# YET ANOTHER REACT DIAGRAM
![](docs/yardPreview.png)
This repository contains a proof of concept of a node-based editor (diagram) written using react and canvas.
## Getting started
- This project uses node.js, make sure you have it installed
- Clone the repo
- Enter the project folder
- Execute the following to install and run the demo
```
npm install
npm start
```
If everything goes well, a browser window should open on http://localhost:3000/ and you should be presented with the demo
#
## Current supported functionality
- Dragging and dropping nodes from the sidebar to the canvas
- Editing nodes via the edit button
- Connecting nodes via their input / output pins
#
Enjoy!