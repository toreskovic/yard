import React, {useState} from 'react';
import './App.css';
import Canvas from './Canvas';

import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal'

function useForceUpdate() {
  let [value, setState] = useState(true);
  return () => setState(!value);
}

let editImg = new Image();
editImg.src = 'EditWhite.png';
var boxes = [];

let dragOp = {
  source: null,
  sourceIndex: 0,
  target: null,
  targetIndex: 0
}

let lastFrameMouseInput = {
  x: 0,
	y: 0,
	mouseDown: false,
	mouseUp: false
};

class pinTemplate {
  source = {box: null, index: 0};
  target = {box: null, index: 0};

  constructor(sourceBox, sourcePinIndex, targetBox, targetPinIndex) {
    this.source = {box: sourceBox, index: sourcePinIndex};
    this.target = {box: targetBox, index: targetPinIndex};
  }
}

class boxTemplate {
  x = 0;
  y = 0;
  width = 200;
  height = 120;
  name = '';

  mouseOver = false;
  mouseOverPinIn = 0;
  mouseOverPinOut = 0;
  mouseOverEdit = false;

  mouseDown = false;
  mouseDownPinIn = 0;
  mouseDownPinOut = 0;
  mouseDownEdit = false;

  boxFill = '#b0b0b0';
  boxFillHover = '#cbcbcb';

  headerFill = '#222266FF';
  headerFillHover = '#333377FF';

  inputPins = [];
  outputPins = [];

  oldState = {};

  constructor(x, y, inputPins = 1, outputPins = 1) {
    this.x = x;
    this.y = y;
  }

  processInput(event, comp) {
    if (event.x < this.x || event.x > this.x + this.width) {
      this.mouseOver = false;
    }
    else if (event.y < this.y || event.y > this.y + this.height) {
      this.mouseOver = false;
    }
    else {
      this.mouseOver = true;
    }

    if (event.mouseUp) {
      this.mouseDown = false;
    }

    let offset = 25;
    let i;

    this.mouseOverPinIn = 0;
    i = 1;
    for (let inputPin of this.inputPins) {
      if (Math.abs(this.x - event.x) < 10 && Math.abs(this.y + i * 30 + offset - event.y) < 10) {
        this.mouseOverPinIn = i;
        break;
      }
      i++;
    }

    this.mouseOverPinOut = 0;
    i = 1;
    for (let outputPin of this.outputPins) {
      if (Math.abs(this.x + this.width - event.x) < 10 && Math.abs(this.y + i * 30 + offset - event.y) < 10) {
        this.mouseOverPinOut = i;
        break;
      }
      i++;
    }

    this.mouseOverEdit = false;
    if (event.x > this.x + this.width - 24 && event.x < this.x + this.width &&
        event.y > this.y && event.y < this.y + 24) {
      this.mouseOverEdit = true;
    }

    if (event.mouseDown) {
      if (this.mouseOverPinOut > 0) {
        dragOp.source = this;
        dragOp.sourceIndex = this.mouseOverPinOut;
      }
      else if (this.mouseOverPinIn > 0) {
        dragOp.target = this;
        dragOp.targetIndex = this.mouseOverPinIn;
      }
      else if (this.mouseOverEdit) {
        this.mouseDownEdit = true;
      }
      else if (this.mouseOver) {
        this.mouseDown = true;
      }
    }
    else if (event.mouseUp) {
      if (this.mouseOverPinIn > 0 && dragOp.source !== null && dragOp.source !== this) {
        dragOp.target = this;
        dragOp.targetIndex = this.mouseOverPinIn;

        let inputPin = dragOp.target.inputPins[dragOp.targetIndex - 1];
        if (inputPin.source.box !== null) {
          inputPin.source.box.outputPins[inputPin.source.index - 1].target = {box: null, index: 0};
        }

        let outputPin = dragOp.source.outputPins[dragOp.sourceIndex - 1];
        if (outputPin.target.box !== null) {
          outputPin.target.box.inputPins[outputPin.target.index - 1].source = {box: null, index: 0};
        }

        dragOp.source.outputPins[dragOp.sourceIndex - 1].target = {box: dragOp.target, index: dragOp.targetIndex};
        dragOp.target.inputPins[dragOp.targetIndex - 1].source = {box: dragOp.source, index: dragOp.sourceIndex};
      }
      else if (this.mouseOverPinOut > 0 && dragOp.target !== null && dragOp.target !== this) {
        dragOp.source = this;
        dragOp.sourceIndex = this.mouseOverPinOut;

        let inputPin = dragOp.target.inputPins[dragOp.targetIndex - 1];
        if (inputPin.source.box !== null) {
          inputPin.source.box.outputPins[inputPin.source.index - 1].target = {box: null, index: 0};
        }

        let outputPin = dragOp.source.outputPins[dragOp.sourceIndex - 1];
        if (outputPin.target.box !== null) {
          outputPin.target.box.inputPins[outputPin.target.index - 1].source = {box: null, index: 0};
        }

        dragOp.source.outputPins[dragOp.sourceIndex - 1].target = {box: dragOp.target, index: dragOp.targetIndex};
        dragOp.target.inputPins[dragOp.targetIndex - 1].source = {box: dragOp.source, index: dragOp.sourceIndex};
      }
      else if (this.mouseOverEdit && this.mouseDownEdit) {
        this.openEditor(comp);
      }
    }

    if (this.mouseDown) {
      this.x += event.x - lastFrameMouseInput.x;
      this.y += event.y - lastFrameMouseInput.y;
    }
  }

  drawBox(ctx) {
    let oldStyle = ctx.fillStyle;

    if (this.mouseOver && this.mouseOverPinIn === 0 && this.mouseOverPinOut === 0) {
      ctx.fillStyle = this.boxFillHover;
    }
    else {
      ctx.fillStyle = this.boxFill;
    }

    ctx.fillRect(this.x, this.y, this.width, this.height);

    if (this.mouseOver && this.mouseOverPinIn === 0 && this.mouseOverPinOut === 0) {
      ctx.fillStyle = this.headerFillHover;
    }
    else {
      ctx.fillStyle = this.headerFill;
    }

    ctx.fillRect(this.x, this.y, this.width, 25);

    if (this.mouseOverEdit) {
      ctx.globalAlpha = 0.8;
    }
    else {
      ctx.globalAlpha = 0.5;
    }

    ctx.drawImage(editImg, this.x + this.width - 22, this.y + 1, 22, 22);
    ctx.globalAlpha = 1.0;

    ctx.fillStyle = '#c0cbdc';
    ctx.font = '20px Arial';
    ctx.fillText(this.name, this.x + 4, this.y + 20);

    ctx.beginPath();

    let offset = 25;

    let i = 1;
    for (let inputPin of this.inputPins) {
      if (this.mouseOverPinIn === i) {
        ctx.fillStyle = this.boxFillHover;
      }
      else {
        ctx.fillStyle = this.boxFill;
      }

      ctx.arc(this.x, this.y + i * 30 + offset, 6, 0, Math.PI * 2, true);
      i++;
    }
    ctx.fill();

    ctx.beginPath();
    i = 1;
    for (let outputPin of this.outputPins) {
      if (this.mouseOverPinOut === i) {
        ctx.fillStyle = this.boxFillHover;
      }
      else {
        ctx.fillStyle = this.boxFill;
      }

      ctx.arc(this.x + this.width, this.y + i * 30 + offset, 6, 0, Math.PI * 2, true);
      i++;
    }
    ctx.fill();

    ctx.fillStyle = oldStyle;
  }
  
  drawLines(ctx) {
    let offset = 25;

    ctx.beginPath();
    for (let outputPin of this.outputPins) {
      if (dragOp.source === this && dragOp.sourceIndex === outputPin.source.index) {
        ctx.moveTo(this.x + this.width, this.y + outputPin.source.index * 30 + offset);
        ctx.bezierCurveTo(this.x + this.width + 150, this.y + outputPin.source.index * 30 + offset, lastFrameMouseInput.x - 150, lastFrameMouseInput.y, lastFrameMouseInput.x, lastFrameMouseInput.y);
      }
      else {
        if (dragOp.target !== null) {
          let src = dragOp.target.inputPins[dragOp.targetIndex - 1].source;
          if (src.box === this && src.index === outputPin.source.index) {
            continue;
          }
        }

        if (outputPin.source.box != null && outputPin.target.box != null) {
          ctx.moveTo(this.x + this.width, this.y + outputPin.source.index * 30 + offset);
          ctx.bezierCurveTo(this.x + this.width + 150, this.y + outputPin.source.index * 30 + offset, outputPin.target.box.x - 150, outputPin.target.box.y + outputPin.target.index * 30 + offset, outputPin.target.box.x, outputPin.target.box.y + outputPin.target.index * 30 + offset);
        }
      }
    }
    for (let inputPin of this.inputPins) {
      if (dragOp.target === this && dragOp.targetIndex === inputPin.target.index) {
        ctx.moveTo(lastFrameMouseInput.x, lastFrameMouseInput.y);
        ctx.bezierCurveTo(lastFrameMouseInput.x + 150, lastFrameMouseInput.y, this.x - 150, this.y + inputPin.target.index * 30 + offset, this.x, this.y + inputPin.target.index * 30 + offset);
      }
    }
    ctx.stroke();
  }

  openEditor(comp) {
    this.oldState.name = this.name;
    comp.onEditBox(this);
    this.mouseOver = false;
    this.mouseOverEdit = false;
    this.mouseOverPinIn = 0;
    this.mouseOverPinOut = 0;
  }

  revertChanges() {
    this.name = this.oldState.name;
  }

  renderEditor(updater) {
    return (
      <label>
        Name:
        <input type="text" 
          value={this.name} 
          onChange={
            (event) => {
              if (event.target.value.length < 13) {
                this.name = event.target.value;
              }
              updater();
            }
          } />
      </label>
    );
  }
}

class boxTemplate1 extends boxTemplate {
  headerFill = '#a22633';
  headerFillHover = '#e43b44';

  name = "Start";

  constructor(x, y) {
    super(x, y);

    this.outputPins = [new pinTemplate(this, 1, null, 1)];
  }
}

class boxTemplate2 extends boxTemplate {
  headerFill = '#265c42';
  headerFillHover = '#3e8948';

  name = "Call";
  phoneNumber = '';

  constructor(x, y) {
    super(x, y);

    this.inputPins = [new pinTemplate(null, 1, this, 1)];
    this.outputPins = [new pinTemplate(this, 1, null, 1)];
  }

  drawBox(ctx) {
    super.drawBox(ctx);

    ctx.fillStyle = '#181425';
    ctx.font = '14px Arial';
    ctx.fillText('Phone Number:', this.x + 8, this.y + 44);
    ctx.fillText(this.phoneNumber, this.x + 8, this.y + 44 + 14);
  }

  openEditor(comp) {
    this.oldState.phoneNumber = this.phoneNumber;
    super.openEditor(comp);
  }

  revertChanges() {
    super.revertChanges();
    this.phoneNumber = this.oldState.phoneNumber;
  }

  renderEditor(updater) {
    return (
      <div>
        <div>
          <div>Name:</div>
          <input type="text" 
            value={this.name} 
            onChange={(event) => {
              if (event.target.value.length < 13) {
                this.name = event.target.value;
              }
              updater();
            }
          }/>
        </div>
        <div>
          <div>Phone number:</div>
          <input type="text" 
            value={this.phoneNumber} 
            onChange={(event) => {
              if (event.target.value.length < 16) {
                this.phoneNumber = event.target.value.replace(/\D/g,''); ;
              }
              updater();
            }
          }/>
        </div>
      </div>
    );
  }
}

class boxTemplate3 extends boxTemplate {
  headerFill = '#124e89';
  headerFillHover = '#0099db';

  name = "End";

  constructor(x, y) {
    super(x, y);

    this.inputPins = [new pinTemplate(null, 1, this, 1)];
  }
}

let menuBoxes = [new boxTemplate1(0, 0), new boxTemplate2(0, 0), new boxTemplate3(0, 0)]

function processInput(event, comp) {
  for (let boxz of boxes) {
    boxz.processInput(event, comp);
  }

  Object.assign(lastFrameMouseInput, event);
  if (event.mouseUp) {
    if (dragOp.source !== null && dragOp.target === null) {
      if (dragOp.source.outputPins[dragOp.sourceIndex - 1].target.box !== null) {
        let trgt = dragOp.source.outputPins[dragOp.sourceIndex - 1].target;
        if (!!trgt.box) {
          trgt.box.inputPins[trgt.index - 1].source = {box: null, index: 0};
        }
      }

      dragOp.source.outputPins[dragOp.sourceIndex - 1].target = {box: null, index: 0};
    }
    else if (dragOp.target !== null && dragOp.source === null) {
      if (dragOp.target.inputPins[dragOp.targetIndex - 1].source.box !== null) {
        let src = dragOp.target.inputPins[dragOp.targetIndex - 1].source;
        if (!!src.box) {
          src.box.outputPins[src.index - 1].target = {box: null, index: 0};
        }
      }

      dragOp.target.inputPins[dragOp.targetIndex - 1].source = {box: null, index: 0};
    }

    dragOp = {
      source: null,
      sourceIndex: 0,
      target: null,
      targetIndex: 0
    }
  }
}

function drawGrid(ctx) {
  let bw = ctx.canvas.width;
  let bh = ctx.canvas.height;
  let d = 30;

  for (let i = 0; i < bw; i += d)
  {
    ctx.moveTo(i, 0);
    ctx.lineTo(i, bh);
  }

  for (let j = 0; j < bh; j += d)
  {
    ctx.moveTo(0, j);
    ctx.lineTo(bw, j);
  }

  ctx.lineWidth = 1;
  let oldStrokeStyle = ctx.strokeStyle;
  ctx.strokeStyle = '#8b9bb466';
  ctx.stroke();
  ctx.strokeStyle = oldStrokeStyle;

  let oldStyle = ctx.fillStyle;
  ctx.fillStyle = '#666666FF';

  oldStrokeStyle = ctx.strokeStyle;
  ctx.strokeStyle = '#262b44';
  ctx.lineWidth = 6;
  
  let curBox = null;

  for (let boxz of boxes) {
    boxz.drawLines(ctx);
  }

  for (let boxz of boxes) {
    boxz.drawBox(ctx);
  }

  ctx.fillStyle = oldStyle;
  ctx.strokeStyle = oldStrokeStyle;
}

function drawMenu1(ctx) {
  menuBoxes[0].drawBox(ctx);
}

function drawMenu2(ctx) {
  menuBoxes[1].drawBox(ctx);
}

function drawMenu3(ctx) {
  menuBoxes[2].drawBox(ctx);
}

function onDrag(event, id) {
  event.dataTransfer.setData("text/plain", id);
}

function allowDrop(event) {
  event.preventDefault();
}

function onDrop(event) {
  event.preventDefault();
  
  let x = event.pageX - event.currentTarget.offsetLeft;
  let y = event.pageY - event.currentTarget.offsetTop;

  let newBox = new menuBoxes[event.dataTransfer.getData("text/plain")].constructor(x, y);
  boxes.push(newBox);
}

const ModalWrapper = ({handleCancel, handleSubmit, show, renderChild}) => {
  let forceUpdate = useForceUpdate();

  return (
    <Modal
      show={show}
      size="sm"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Editor
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {renderChild(forceUpdate)}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={handleCancel}>Cancel</Button>
        <Button onClick={handleSubmit}>Submit</Button>
      </Modal.Footer>
    </Modal>
  );
}

class Diagram extends React.Component {
  constructor(props) {
    super(props);

    this.onEdit = this.onEdit.bind(this);
    this.cancelModal = this.cancelModal.bind(this);
    this.submitModal = this.submitModal.bind(this);
  }

  onEdit(box) {
    this.setState({isModalActive: true, box: box});
  }

  cancelModal() {
    if (this.state && this.state.box) {
      this.state.box.revertChanges();
    }

    this.setState({isModalActive: false, box: null});
  }

  submitModal() {
    this.setState({isModalActive: false, box: null});
  }

  render() {
    let modalChildren = (updater) => {};
    if (this.state && this.state.box) {
      modalChildren = this.state.box.renderEditor.bind(this.state.box);
    }

    return (
      <div className='contentFull'>
        <ModalWrapper
          show={this.state ? this.state.isModalActive : false}
          handleCancel={this.cancelModal}
          handleSubmit={this.submitModal}
          renderChild={modalChildren}/>
				<div className='contentL'>
          <div className='container'>
            <div className = 'boxHolder'
              draggable
              onDragStart={(event) => {onDrag(event, 0);}}
              >
              <Canvas
                loop={false}
                clear={true}
                paint={drawMenu1}
                processInput={(e) => {}}/>
            </div>
            <div className = 'boxHolder'
              draggable
              onDragStart={(event) => {onDrag(event, 1);}}
              >
              <Canvas
                loop={false}
                clear={true}
                paint={drawMenu2}
                processInput={(e) => {}}/>
            </div>
            <div className = 'boxHolder'
              draggable
              onDragStart={(event) => {onDrag(event, 2);}}
              >
              <Canvas
                loop={false}
                clear={true}
                paint={drawMenu3}
                processInput={(e) => {}}/>
            </div>
          </div>
        </div>
        <div className='contentR'
          onDrop={onDrop}
          onDragOver={allowDrop}
          >
          <Canvas
            loop={true}
            clear={true}
            paint={drawGrid}
            processInput={processInput}
            onEdit={this.onEdit}/>
        </div>
			</div>
    );
  }
}

function App() {
  return (
    <div className="App">
      <Diagram/>
    </div>
  );
}

export default App;
