import React from 'react';
import PropTypes from 'prop-types'

class Canvas extends React.Component {
	constructor(props) {
		super(props);
		this.draw = this.draw.bind(this);
		this.onMouseMove = this.onMouseMove.bind(this);
		this.onMouseDown = this.onMouseDown.bind(this);
		this.onMouseUp = this.onMouseUp.bind(this);
		this.updateState = this.updateState.bind(this);
		this.onEditBox = this.onEditBox.bind(this);

		this.input = {
			x: 0,
			y: 0,
			mouseDown: false,
			mouseUp: false
		};
	}

	componentDidMount() {
		this.updateState();

		window.addEventListener('resize', this.updateState);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateState);
	}

	updateState() {
		const view = this.refs.view;
		const ratio = 1;
		const width = (view.clientWidth) | 0;
		const height = (view.clientHeight) | 0;
		const resizing = false;
		this.setState({ width, height, ratio, resizing });
	}

	onEditBox(box) {
		this.props.onEdit(box);
	}

	componentDidUpdate(prevProps) {
		this.draw();
	}

	onMouseMove(e) {
		this.input.x = e.pageX - e.currentTarget.offsetLeft;
		this.input.y = e.pageY - e.currentTarget.offsetTop;

		this.props.processInput(this.input, this);
	}

	onMouseDown(e) {
		this.input.mouseDown = true;

		this.props.processInput(this.input, this);
		this.input.mouseDown = false;
	}

	onMouseUp(e) {
		this.input.mouseUp = true;

		this.props.processInput(this.input, this);
		this.input.mouseUp = false;
	}

	draw() {
		if (this.state) {
			const { width, height, ratio } = this.state;
			const canvas = this.refs.canvas;
			if (!!canvas) {
				let context = canvas.getContext('2d');
				context.width = width;
				context.height = height;
				context.pixelRatio = ratio;
				if (this.props.clear) {
					context.clearRect(0, 0, canvas.width, canvas.height);
				}

				context.beginPath();
				context.beginPath();
				this.props.paint(context);
			}
		}

		if (this.props.loop) {
			window.requestAnimationFrame(this.draw);
		}
	}

	render() {
		const canvas = this.state ? (
			<canvas
				ref='canvas'
				width={this.state.width}
				height={this.state.height}
				style={{
					width: '100%',
					height: '100%',
				}} onMouseMove = {this.onMouseMove}
				onMouseDown = {this.onMouseDown}
				onMouseUp = {this.onMouseUp} />
		) : null;

		return (
			<div
				ref='view'
				className={this.props.className ? this.props.className : 'view'}
				style={{
					width: '100%',
					height: '100%',
				}}>
				{canvas}
			</div>
		);
	}
}

Canvas.propTypes = {
	paint: PropTypes.func.isRequired,
	processInput: PropTypes.func.isRequired,
	onEdit: PropTypes.func,
	clear: PropTypes.bool,
	loop: PropTypes.bool,
	className: PropTypes.string,
	style: PropTypes.object,
};

export default Canvas;